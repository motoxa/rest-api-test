<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| [POST]   auth/login
| [POST]   auth/signup
| [POST]   auth/logout
| [GET]    users/
| [GET]    users/(:int|me)
| [POST]   users/(:int|me)
| [POST]   users/(:int|me)/like
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'AuthController@logout');
    });
});

Route::group(['prefix' => 'users', 'middleware' => 'auth:api'], function () {
    Route::pattern('user_id', '([0-9]+|me)');

    Route::get('/', 'UsersController@allUsers');
    Route::get('{user_id}', 'UsersController@user');
    Route::post('{user_id}', 'UsersController@update');
    Route::post('{user_id}/like', 'UsersController@like');
});
