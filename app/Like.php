<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id'
    ];

    /**
     * Get the owning likeable model
     */
    public function likeable()
    {
        return $this->morphTo();
    }
}
