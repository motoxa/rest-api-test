<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use App\Like;

class UsersController extends Controller
{
    /**
     * Get all users
     *
     * @return [json] Users collection
     */
    public function allUsers(Request $request)
    {
        $users = User::select('name','created_at')->get();

        return response()->json($users);
    }

    /**
     * Get specified user info
     *
     * @param  mixed $user_id
     * @return [json] User object
     */
    public function user(Request $request, $user_id)
    {
        if ($user_id == 'me') {
            $user_id = $request->user()->id;
        }

        $user = User::whereId($user_id)->with('likes')->get();

        return response()->json($user);
    }

    /**
     * Update user info
     *
     * @param  mixed $user_id
     * @param  [string] name
     * @return [string] message
     */
    public function update(Request $request, $user_id)
    {
        if ($user_id == 'me') {
            $user_id = $request->user()->id;
        }

        $request->validate([
            'name' => 'required|string'
        ]);

        $user = User::find($user_id);
        $user->name = $request->name;
        $user->save();

        return response()->json([
            'message' => 'User information successfully updated!'
        ], 201);
    }

    /**
     * Like user
     *
     * @param  mixed $user_id
     * @return [string] message
     */
    public function like(Request $request, $user_id)
    {
        $owner_id = $request->user()->id;

        if ($user_id == 'me') {
            $user_id = $owner_id;
        }

        $validatorInput = ['id' => $user_id];
        $validatorRules = ['id' => function ($attribute, $value, $fail) use ($owner_id) {
            $exists = Like::whereHasMorph('likeable', [User::class], function ($query) use ($owner_id) {
                $query->where('owner_id', $owner_id);
            })->get();
            if ($exists) {
                $fail('Item has already been liked by this user.');
            }
        }];
        Validator::make($validatorInput, $validatorRules)->validate();

        $like = new Like(['owner_id' => $owner_id]);
        User::find($user_id)->likes()->save($like);

        return response()->json([
            'message' => 'User liked successfully!'
        ], 201);
    }
}
